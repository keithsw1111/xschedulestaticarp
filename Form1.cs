﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;
using System.Diagnostics;
using System.Net.NetworkInformation;

namespace xLightsStaticARP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            ValidateWindow();
        }

        // The max number of physical addresses.
        const int MAXLEN_PHYSADDR = 8;

        // Define the MIB_IPNETROW structure.
        [StructLayout(LayoutKind.Sequential)]
        struct MIB_IPNETROW
        {
            [MarshalAs(UnmanagedType.U4)]
            public int dwIndex;
            [MarshalAs(UnmanagedType.U4)]
            public int dwPhysAddrLen;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac0;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac1;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac2;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac3;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac4;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac5;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac6;
            [MarshalAs(UnmanagedType.U1)]
            public byte mac7;
            [MarshalAs(UnmanagedType.U4)]
            public int dwAddr;
            [MarshalAs(UnmanagedType.U4)]
            public int dwType;
        }

        // Declare the GetIpNetTable function.
        [DllImport("IpHlpApi.dll")]
        [return: MarshalAs(UnmanagedType.U4)]
        static extern int GetIpNetTable(
           IntPtr pIpNetTable,
           [MarshalAs(UnmanagedType.U4)]
         ref int pdwSize,
           bool bOrder);

        [DllImport("IpHlpApi.dll", SetLastError = true, CharSet = CharSet.Auto)]
        internal static extern int FreeMibTable(IntPtr plpNetTable);

        // The insufficient buffer error.
        const int ERROR_INSUFFICIENT_BUFFER = 122;

        private Dictionary<string, string> GetARP()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();
            // The number of bytes needed.
            int bytesNeeded = 0;

            // The result from the API call.
            int result = GetIpNetTable(IntPtr.Zero, ref bytesNeeded, false);

            // Call the function, expecting an insufficient buffer.
            if (result != ERROR_INSUFFICIENT_BUFFER)
            {
                return res;
            }

            // Allocate the memory, do it in a try/finally block, to ensure
            // that it is released.
            IntPtr buffer = IntPtr.Zero;

            // Try/finally.
            try
            {
                // Allocate the memory.
                buffer = Marshal.AllocCoTaskMem(bytesNeeded);

                // Make the call again. If it did not succeed, then
                // raise an error.
                result = GetIpNetTable(buffer, ref bytesNeeded, false);

                // If the result is not 0 (no error), then throw an exception.
                if (result != 0)
                {
                    return res;
                }

                // Now we have the buffer, we have to marshal it. We can read
                // the first 4 bytes to get the length of the buffer.
                int entries = Marshal.ReadInt32(buffer);

                // Increment the memory pointer by the size of the int.
                IntPtr currentBuffer = new IntPtr(buffer.ToInt64() +
                   Marshal.SizeOf(typeof(int)));

                // Allocate an array of entries.
                MIB_IPNETROW[] table = new MIB_IPNETROW[entries];

                // Cycle through the entries.
                for (int index = 0; index < entries; index++)
                {
                    // Call PtrToStructure, getting the structure information.
                    table[index] = (MIB_IPNETROW)Marshal.PtrToStructure(new
                       IntPtr(currentBuffer.ToInt64() + (index *
                       Marshal.SizeOf(typeof(MIB_IPNETROW)))), typeof(MIB_IPNETROW));
                }

                for (int index = 0; index < entries; index++)
                {
                    MIB_IPNETROW row = table[index];

                    res[new IPAddress(BitConverter.GetBytes(row.dwAddr)).ToString()] =
                        row.mac0.ToString("X2") + '-' +
                        row.mac1.ToString("X2") + '-' +
                        row.mac2.ToString("X2") + '-' +
                        row.mac3.ToString("X2") + '-' +
                        row.mac4.ToString("X2") + '-' +
                        row.mac5.ToString("X2");
                }
            }
            finally
            {
                // Release the memory.
                FreeMibTable(buffer);
            }

            return res;
        }

        private void ValidateWindow()
        {
            if (Directory.Exists(textBox1.Text) &&
                File.Exists(textBox1.Text + "\\xlights_networks.xml"))
                {
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = textBox1.Text;

            if (folderBrowserDialog1.ShowDialog(this) == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }

            ValidateWindow();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            ValidateWindow();
        }

        private bool IsIP(string ip)
        {
            IPAddress junk = new IPAddress(0);
            return IPAddress.TryParse(ip, out junk);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(textBox1.Text + "\\xlights_networks.xml");
            List<string> ips = new List<string>();

            foreach (XmlNode n in doc.ChildNodes)
            {
                if (n.NodeType != XmlNodeType.Comment)
                {
                    foreach (XmlNode nn in n.ChildNodes)
                    {
                        if (nn.NodeType != XmlNodeType.Comment && nn.Name == "network" && nn.Attributes != null && nn.Attributes["ComPort"] != null)
                        {
                            string ip = nn.Attributes["ComPort"].Value;
                            if (IsIP(ip))
                            {
                                if (!ips.Contains(ip))
                                {
                                    ips.Add(ip);

                                    Ping pingSender = new Ping();
                                    PingOptions options = new PingOptions();

                                    // Use the default Ttl value which is 128,
                                    // but change the fragmentation behavior.
                                    options.DontFragment = true;

                                    // Create a buffer of 32 bytes of data to be transmitted.
                                    string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                                    byte[] buffer = Encoding.ASCII.GetBytes(data);
                                    int timeout = 120;
                                    PingReply reply = pingSender.Send(ip, timeout, buffer, options);
                                    if (reply.Status == IPStatus.Success)
                                    {
                                        Debug.WriteLine("Ping successful " + ip);
                                    }
                                    else
                                    {
                                        Debug.WriteLine("Ping failed " + ip);
                                    }
                                }
                            }
                        }
                        else if (nn.NodeType != XmlNodeType.Comment && nn.Name == "Controller" && nn.Attributes != null && nn.Attributes["IP"] != null)
                        {
                            string ip = nn.Attributes["IP"].Value;
                            if (IsIP(ip))
                            {
                                if (!ips.Contains(ip))
                                {
                                    ips.Add(ip);

                                    Ping pingSender = new Ping();
                                    PingOptions options = new PingOptions();

                                    // Use the default Ttl value which is 128,
                                    // but change the fragmentation behavior.
                                    options.DontFragment = true;

                                    // Create a buffer of 32 bytes of data to be transmitted.
                                    string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                                    byte[] buffer = Encoding.ASCII.GetBytes(data);
                                    int timeout = 120;
                                    PingReply reply = pingSender.Send(ip, timeout, buffer, options);
                                    if (reply.Status == IPStatus.Success)
                                    {
                                        Debug.WriteLine("Ping successful " + ip);
                                    }
                                    else
                                    {
                                        Debug.WriteLine("Ping failed " + ip);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Dictionary<string, string> arps = GetARP();

            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();

            List<string> inUseAdapters = new List<string>();
            foreach (string ip in ips)
            {
                string ad = GetAdapterForIp(ip, adapters);
                if (ad != "" && !inUseAdapters.Contains(ad))
                {
                    inUseAdapters.Add(ad);
                }
            }

            string filename = textBox1.Text + "\\StaticARP.cmd";
            StreamWriter file = new StreamWriter(filename);

            file.WriteLine("REM Run this command file as administrator to set up permanent ARP entries\n");

            file.WriteLine("REM Setting these entries can eliminate some micropauses when playing back");
            file.WriteLine("REM sequences using xSchedule where the delays are due to failed controllers");
            file.WriteLine("REM and possibly some other network gremlins");

            file.WriteLine("\nREM Beware if a controllers IP address changes you must remove these entries");
            file.WriteLine("REM and regenerate them or packets will not reach the controller.");

            file.WriteLine("\nREM Remove any previous entries");
            foreach (string ad in inUseAdapters)
            {
                file.WriteLine("netsh interface ipv4 delete neighbors \"" + ad + "\"");
            }

            file.WriteLine("\nREM Add new entries");
            foreach (string ip in ips)
            {
                string ad = GetAdapterForIp(ip, adapters);
                string mac = GetMACForIP(ip, arps);
                Debug.WriteLine(ip + " : " + mac + " : " + ad);

                file.WriteLine("netsh interface ipv4 add neighbors \""+ ad +"\" "+ ip +" " + mac);
            }

            file.WriteLine("\nREM Show the user the entries now in force");
            foreach (string ad in inUseAdapters)
            {
                file.WriteLine("netsh interface ipv4 show neighbors \"" + ad + "\"");
            }

            file.WriteLine("\npause");

            file.Close();

            Process.Start("notepad.exe", "\"" + filename + "\"");

            filename = textBox1.Text + "\\ClearStaticARP.cmd";
            file = new StreamWriter(filename);

            file.WriteLine("REM Run this command file as administrator to clear permanent ARP entries\n");

            file.WriteLine("\nREM Remove any previous entries");
            foreach (string ad in inUseAdapters)
            {
                file.WriteLine("netsh interface ipv4 delete neighbors \"" + ad + "\"");
            }

            file.WriteLine("\nREM Show the user the entries now in force");
            foreach (string ad in inUseAdapters)
            {
                file.WriteLine("netsh interface ipv4 show neighbors \"" + ad + "\"");
            }

            file.WriteLine("\npause");

            file.Close();
        }

        private string GetAdapterForIp(string ip, NetworkInterface[] adapters)
        {
            foreach (NetworkInterface ni in adapters)
            {
                foreach  (var ipp in ni.GetIPProperties().UnicastAddresses)
                {
                    if (ipp.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        if (SameSubnet(ipp.Address.Address, ipp.IPv4Mask.Address, IPAddress.Parse(ip).Address))
                        {
                            return ni.Name;
                        }
                    }
                }
            }

            return "";
        }

        private string GetMACForIP(string ip, Dictionary<string, string> arps)
        {
            if (arps.ContainsKey(ip)) return arps[ip];
            return "";
        }

        private bool SameSubnet(long localAddress, long localMask, long ip)
        {
            return (localAddress & localMask) == (ip & localMask);
        }
    }
}
