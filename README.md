# xLightsStaticARP

This utility is designed to work with xLights on Windows.

When running xSchedule we have observed micro-pauses occasionaly in the network which are closely correlated to ARP lookups. These lock the network until they are resolved or time out.

This utility is pointed at the xLights show folder and then scans your network for your controllers and builds a .CMD file which you can then run as an administrator to set permanent ARP mappings between your controllers IP addresses and their MAC addresses.

This is not hard to do but it has a big downside if at any time you replace the controller or you change a controllers IP address. Weird things can happen.

If that happens either re-generate the script and run it or run the following command to clear out all permanent mappings.

netsh interface ipv4 delete neighbors "<insert your adapter name here>"
